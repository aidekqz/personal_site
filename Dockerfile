FROM nginx:alpine
ARG SITE_FQDN=test.ru
RUN rm -rf /usr/share/nginx/html/*
COPY app /usr/share/nginx/html
RUN sed -i 's/{{ domain_name }}/'"$SITE_FQDN"'/g' /usr/share/nginx/html/index.html
